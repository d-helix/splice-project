import requests
import os
import sys
import re
import subprocess

def get_canonical_transcript(gene):
    """
    Searches refseq data to find the canonical transcript for a gene
    :param gene: (string) HGNC approved gene symbol
    :return: (string) canonical transcript
    """
    transcript = ''
    with open("data/canonical.txt") as fh:
        for line in fh:
            line = line.split()
            if line[1] == gene:
                transcript = line[2]
                break

    return transcript

def get_sequence_by_http(chromosome=None, start_coord=None, end_coord=None, genome_version='hg19',
                         five_padding='0', three_padding='0', case='upper', strand='+'):
    """
    Retrieves the DNA sequence of a given gene by building a url and making an http request to ucsc.edu
    :param chromosome: (string) the chromosome number in which the desired gene is located
    :param start_coord: (string) the genomic coordinate of the start of the gene
    :param end_coord: (string) the genomic coordinate of the end of the gene
    :param genome_version: (string) the desired human genome(hg) version, hg19 is the default
    :param five_padding: (string) number of bases upstream of the gene to add onto the sequence, default = 0
    :param three_padding:  (string) number of bases downstream of the gene to add onto the sequence, default = 0
    :param case: (string) receive the sequence in upper or lower case, default = lower
    :param strand: (string) + or - indicating the forward or reverse strand
    :return: sequence: (string) the DNA sequence of the gene
    """
    url = "http://genome.ucsc.edu/cgi-bin/hgc?" \
          "hgsid=435121445_lTFOG4ajIZpJhccR0mAxvji2grNJ" \
          "&g=htcGetDna2&table=&i=mixed" \
          "&l=" + start_coord + "&r=" + end_coord + \
          "&getDnaPos=" + chromosome + \
          ":" + start_coord + "-" + end_coord + \
          "&db=" + genome_version + \
          "&hgSeq.cdsExon=1" \
          "&hgSeq.padding5=" + five_padding + \
          "&hgSeq.padding3=" + three_padding + \
          "&hgSeq.casing=" + case + \
          "&boolshad.hgSeq.maskRepeats=0&hgSeq.repMasking=lower"
    if strand == '-':
        url += "&hgSeq.revComp=on"
    url += "&boolshad.hgSeq.revComp=0" \
           "&submit=get+DNA"

    response = requests.get(url)
    if response.status_code > 200:
        print "Received an ERROR status code from UCSC"
        print "Status Code: " + str(response.status_code)
        sys.exit()
    html = response.content

    record = False
    header = None
    sequence = ''
    listed = html.split('\n')
    for line in listed:
        if line == "<PRE>":
            record = True
            continue
        if line == "</PRE>":
            break
        if record:
            if not header:
                header = line.strip()
                continue
            sequence += line.strip()

    if sequence == '':
        sys.exit("Failed to retrieve sequence from UCSC")

    return sequence

def get_refseq_data(gene_name):
    """
    Retrieves data for a gene from a refseq data file
    :param gene_name: (string) HGNC approved gene symbol
    :return: (list) gene name, chromosome, strand
             (list) information for each of the gene's transcripts
    """
    file_directory = os.getcwd()
    with open(file_directory + '/data/refSeqData') as fh:
        tx_list = []
        for line in fh:
            line = line.strip().split()
            if line[12] == gene_name:
                tx_list.append(line)
        if len(tx_list) == 0:
            sys.exit('Gene not found, please check gene name')

        rs = tx_list[0]
        gene_info = [rs[12], rs[2], rs[3]]

    return gene_info, tx_list

def map_sequence(seq, start_pos, end_pos, strand):
    """
    Maps chromosome positions to the corresponding nucleotide
    :param seq: (string) dna sequence
    :param start_pos: (string) chromosome position
    :param end_pos: (string) chromosome position
    :param strand: (string) strand direction, + or -
    :return: (dictionary) key, chromosome position -> value, nucleotide
    """
    mapped_seq = {}
    if strand == '+':
        pos = int(start_pos)
        for nucleotide in seq:
            mapped_seq[pos] = nucleotide
            pos += 1
    if strand == '-':
        pos = int(end_pos)
        for nucleotide in seq:
            mapped_seq[pos] = nucleotide
            pos -= 1

    return mapped_seq

def map_cdna(transcript, gene):
    """
    Maps cdna positions to the corresponding chromosome position
    :param transcript: (transcript object)
    :param gene: (gene object)
    :return: (dictionary) key, cdna position -> value, chromosome position
    """
    chrom_to_cdna = {}
    cdna_to_chrom = {}
    pos = 1

    if gene.strand == '+':
        for exon in transcript.exons:
            if exon.frame == '-1':
                continue
            elif pos == 1:
                for i in range(transcript.cds_start, exon.end + 1):
                    cdna_to_chrom[pos] = i
                    chrom_to_cdna[i] = pos
                    pos += 1
            else:
                for i in range(exon.start, exon.end + 1):
                    if i > transcript.cds_end:
                        break
                    cdna_to_chrom[pos] = i
                    chrom_to_cdna[i] = pos
                    pos += 1

    elif gene.strand == '-':
        for exon in transcript.exons:
            if exon.frame == '-1':
                continue
            elif pos == 1:
                for i in range(transcript.cds_start, exon.end - 1, -1):
                    cdna_to_chrom[pos] = i
                    chrom_to_cdna[i] = pos
                    pos += 1
            else:
                for i in range(exon.start, exon.end - 1, -1):
                    if i < transcript.cds_end:
                        break
                    cdna_to_chrom[pos] = i
                    chrom_to_cdna[i] = pos
                    pos += 1

    return cdna_to_chrom, chrom_to_cdna

def get_cdna_sequence(cdna, seq_map):
    """
    Builds a cdna sequence
    :param cdna: (dictionary) key, cdna position -> value, chromosome position
    :param seq_map: (dictionary) key, chromosome position -> value, nucleotide
    :return: (string) cdna nucleotide sequence
    """
    sequence = ''
    for i in range(1, len(cdna) + 1):
        sequence += seq_map[cdna[i]]

    return sequence

def build_protein(sequence):
    """
    Builds a protein from a dna sequence
    :param sequence: (string) dna sequence
    :return: (string) sequence of amino acids
    """
    aa_map = {"ATT": "I", "ATC": "I", "ATA": "I", "CTT": "L", "CTC": "L", "CTA": "L", "CTG": "L", "TTA": "L", "TTG": "L",
              "GTT": "V", "GTC": "V", "GTA": "V", "GTG": "V", "TTT": "F", "TTC": "F", "ATG": "M", "TGT": "C", "TGC": "C",
              "GCT": "A", "GCC": "A", "GCA": "A", "GCG": "A", "GGT": "G", "GGC": "G", "GGA": "G", "GGG": "G", "CCT": "P",
              "CCC": "P", "CCA": "P", "CCG": "P", "ACT": "T", "ACC": "T", "ACA": "T", "ACG": "T", "TCT": "S", "TCC": "S",
              "TCA": "S", "TCG": "S", "AGT": "S", "AGC": "S", "TAT": "Y", "TAC": "Y", "TGG": "W", "CAA": "Q", "CAG": "Q",
              "AAT": "N", "AAC": "N", "CAT": "H", "CAC": "H", "GAA": "E", "GAG": "E", "GAT": "D", "GAC": "D", "AAA": "K",
              "AAG": "K", "CGT": "R", "CGC": "R", "CGA": "R", "CGG": "R", "AGA": "R", "AGG": "R", "TAA": "X", "TAG": "X",
              "TGA": "X"}
    codons = re.findall(r"...", sequence)
    protein = ''
    for codon in codons:
        protein += aa_map[codon]

    return protein

def maxentscan_score(acceptor=False, donor=False, to_score=None):
    """
    Gives splice sites a score using MaxEntScan
    :param acceptor: (bool) is the site an acceptor site
    :param donor: (bool) is the site a donor site
    :param to_score: (list) sites to score
    :return: (list) sites and their scores
    """
    maxent_directory = "external_resources/maxent/"
    totest = open(maxent_directory + "torun", 'w')
    for entry in to_score:
        totest.write(entry + '\n')
    totest.close()
    if acceptor:
        output = subprocess.check_output(["perl", maxent_directory + "score5.pl", maxent_directory + "torun"])
    elif donor:
        output = subprocess.check_output(["perl", maxent_directory + "score3.pl", maxent_directory + "torun"])
    output = output.split('\n')

    return output
