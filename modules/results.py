import data

class SpliceResult:
    def __init__(self, variant, gene, config, args):
        self.gene = gene
        self.variant = variant
        self.config = config
        self.args = args
        self.ctx_name = data.get_canonical_transcript(gene.gene_name)
        self.ctx = None
        self.threshold = config.getfloat("splice_scoring_tool", "threshold")

        self.transcripts = []
        for tx in variant.tx_hits:
            result = SpliceTxResult(tx, variant, self)
            self.transcripts.append(result)
            if tx.tx.tx_name == self.ctx_name:
                self.ctx = result

    def print_results(self):
        """

        """
        to_print = (
            "Results for " + self.args.gene + ' ' + self.args.variant + '\n' +
            "  Chromosome: " + self.gene.chromosome + '\n' +
            "  Strand: " + self.gene.strand + '\n' +
            "  Total # of transcripts: " + str(len(self.gene.transcripts)) + '\n' +
            (("  " + str(len(self.variant.skips)) + " transcripts skip this exon" + '\n' + '\n')
                if len(self.variant.skips) > 0 else '\n')
        )

        to_print += (
            "Summary of the canonical transcript shown below. Results for each transcripts are found in the " +
            self.config.get("output", "output_file") + " file" + '\n' +
            "  Canonical Transcript: " + self.ctx_name + '\n' +
            "  Total # of exons: " + str(len(self.ctx.tx.tx.exons)) + '\n' +
            "  Splice variant occurs in exon " + str(self.ctx.tx.exon.exon_number) + " " +
            ("donor site" if self.variant.donor else "acceptor site") + '\n' +
            "  Threshold: " + str(self.threshold) + '\n' +
            "    Wild-type splice site: " + str(self.ctx.tx.wt_score) + " " + self.ctx.tx.wt_site + '\n' +
            "    Variant splice site: " + str(self.ctx.tx.var_score) + " " + self.ctx.tx.var_site + '\n'
        )

        if self.ctx.downstream_sites_exceed_threshold:
            to_print += "    Downstream sites (distance downstream, score, site):\n"
            for site in self.ctx.downstream_sites_exceed_threshold:
                to_print += "      " + str(site[1]) + " bp, " + str(site[0]) + ", " + str(site[2]) + '\n'

        if self.ctx.upstream_sites_exceed_threshold:
            to_print += "    Upstream sites (distance upstream, score, site):\n"
            for site in self.ctx.upstream_sites_exceed_threshold:
                to_print += "      " + str(site[1]) + " bp, " + str(site[0]) + ", " + str(site[2]) + '\n'

        print to_print

    def write_to_file(self, config):
        results_file = open(config.get("output", "output_file"), 'w')
        header = 'gene\tchr\tstrand\ttranscript\t'
        return 1

class SpliceTxResult:
    def __init__(self, tx, variant, splice_result):
        """

        :param tx:
        :return:
        """
        self.tx = tx
        self.wt_exceeds_threshold = False
        self.var_exceeds_threshold = False
        self.upstream_sites_exceed_threshold = []
        self.downstream_sites_exceed_threshold = []
        self.site_destroyed = False

        frame_mod = variant.var_cdna_pos % 3

        if tx.var_score > splice_result.threshold:
            # In this case there will not be upstream and downstream scores
            self.var_exceeds_threshold = True
        if tx.wt_score > splice_result.threshold:
            self.wt_exceeds_threshold = True

        if not self.wt_exceeds_threshold:
            self.site_destroyed = True

        if self.site_destroyed:
            upstream = 0
            downstream = 1

            if variant.acceptor:
                upstream = upstream + (self.tx.acceptor_exon_bases + self.tx.acceptor_intron_bases)
                # Find upstream sites that exceed the threshold
                for site in reversed(self.tx.upstream_scores):
                    if site[0] > splice_result.threshold:
                        self.upstream_sites_exceed_threshold.append((site[0], upstream,
                                                                    (site[1][:self.tx.acceptor_intron_bases].lower() +
                                                                     site[1][self.tx.acceptor_intron_bases:].upper())))
                    upstream += 1

                # Find downstream sites that exceed the threshold
                for site in self.tx.downstream_scores:
                    if site[0] > splice_result.threshold:
                        self.downstream_sites_exceed_threshold.append((site[0], downstream,
                                                                      (site[1][:self.tx.acceptor_intron_bases].lower() +
                                                                       site[1][self.tx.acceptor_intron_bases:].upper())))
                    downstream += 1

            elif variant.donor:
                # Find upstream sites that exceed the threshold
                upstream = upstream + (self.tx.donor_exon_bases + self.tx.donor_intron_bases)
                for site in reversed(self.tx.upstream_scores):
                    if site[0] > splice_result.threshold:
                        self.upstream_sites_exceed_threshold.append((site[0], upstream,
                                                                    (site[1][:self.tx.donor_exon_bases].upper() +
                                                                     site[1][self.tx.donor_exon_bases:].lower())))
                    upstream += 1

                # Find downstream sites that exceed the threshold
                for site in self.tx.downstream_scores:
                    if site[0] > splice_result.threshold:
                        self.downstream_sites_exceed_threshold.append((site[0], downstream,
                                                                      (site[1][:self.tx.donor_exon_bases].upper() +
                                                                       site[1][self.tx.donor_exon_bases:].lower())))
                    downstream += 1
