import data

class Gene:
    def __init__(self, gene_info, tx_list):
        """
        Constructor to build a gene object
        :param gene_info: (list) gene name, chromosome, strand
        :param tx_list: (list) information for each of the gene's transcripts
        :return: (gene object)
        """
        self.gene_name = gene_info[0]
        self.chromosome = gene_info[1]
        self.strand = gene_info[2]
        self.transcripts = []
        for tx in tx_list:
            self.transcripts.append(Transcript(self, tx))

    def print_gene(self):
        """
        Prints information found in the refseq file for a gene
        """
        print self.gene_name
        print self.chromosome
        print 'Strand: ' + self.strand
        print '  Transcripts:'
        for tx in self.transcripts:
            print '  ' + tx.tx_name
            print '  tx start: ' + tx.tx_start
            print '  tx end:   ' + tx.tx_end
            print '  cds start: ' + str(tx.cds_start)
            print '  cds_end:   ' + str(tx.cds_end)
            print '    Exons:'
            for ex in tx.exons:
                print ('     ' + str(ex.exon_number) + ': ' + str(ex.start) + ' - ' +
                       str(ex.end) + '    frame: ' + ex.frame + '  length: ' + str(ex.length))

class Transcript:
    def __init__(self, gene, tx):
        """
        Constructor to build a Transcript object
        :param gene: (gene object)
        :param tx: (list of strings) transcript data
        :return: (transcript object)
        """
        self.tx_name = tx[1]
        self.tx_start = str(int(tx[4]) + 1)
        self.tx_end = tx[5]
        self.tx_sequence = data.get_sequence_by_http(chromosome=gene.chromosome, start_coord=self.tx_start,
                                                     end_coord=self.tx_end, strand=gene.strand)
        self.mapped_sequence = data.map_sequence(self.tx_sequence, self.tx_start, self.tx_end, gene.strand)
        self.cds_start = int(tx[6]) + 1
        self.cds_end = int(tx[7])
        self.no_amino_acids = 0
        if gene.strand == '-':
            self.tx_start, self.tx_end = self.tx_end, self.tx_start
            self.cds_start, self.cds_end = self.cds_end, self.cds_start

        self.exons = []
        exon_starts = tx[9].split(',')[:-1]
        exon_ends = tx[10].split(',')[:-1]
        exon_frames = tx[15].split(',')[:-1]
        exon_count = int(tx[8])
        exon_range = list(range(exon_count))
        if gene.strand == '-':
            exon_range = list(reversed(exon_range))
        exon_num = 1
        for i in exon_range:
            self.exons.append(
                Exon(gene, exon_starts[i], exon_ends[i], exon_frames[i], exon_num))
            exon_num += 1

        self.cdna_to_chrom, self.chrom_to_cdna = data.map_cdna(self, gene)
        self.cdna_sequence = data.get_cdna_sequence(self.cdna_to_chrom, self.mapped_sequence)
        self.protein = data.build_protein(self.cdna_sequence)

class Exon:
    def __init__(self, gene, start, end, frame, exon_num):
        """
        Constructor to build an Exon object
        :param gene: (gene object)
        :param start: (string) start position of the exon
        :param end: (string) ending position of the exon
        :param frame: (string) frame that the exon is in -1, 0, 1, 2 (-1 indicates that the exon is non coding)
        :param exon_num: (string) exon number in the transcript
        :return: (exon object)
        """
        if gene.strand == '+':
            self.start = int(start) + 1
            self.end = int(end)
        elif gene.strand == '-':
            self.start = int(end)
            self.end = int(start) + 1
        self.frame = frame
        self.length = abs(int(end) - int(start))
        self.exon_number = exon_num