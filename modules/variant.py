import data

class SpliceVariant:
    def __init__(self, config, gene, cdna_pos, distance, wt_nuc, var_nuc):
        """
        Constructor for a splice specific variant object
        :param config: (config object)
        :param gene: (gene object)
        :param cdna_pos: (string) cdna location of the variant
        :param distance: (string) distance from the above cdna location (if variant is intronic)
        :param wt_nuc: (string) wild type nucleotide at the variant position
        :param var_nuc: (string) resulting variant nucleotide change
        :return: (Splice Variant object)
        """
        self.config = config
        self.gene = gene
        self.var_cdna_pos = cdna_pos
        self.distance_from_exon = distance
        self.wt_nuc = wt_nuc
        self.var_nuc = var_nuc
        self.donor = False
        self.acceptor = False
        self.wt_sites = {}
        self.variant_sites = {}
        self.skips, hits, hit_exons = self.find_skipped_transcripts()
        self.tx_hits = []
        idx = 0
        for hit in hits:
            self.tx_hits.append(SpliceVariantData(self, hit, hit_exons[idx]))
            idx += 1

    def find_skipped_transcripts(self):
        """
        Finds variants that have exons in the area of the variant and those that skip it
        :return: (list of strings) skips, transcripts that are skipped
                 (list of transcript objects) results,  transcripts that have exons affected by the splice variant
                 (list of exon objects) hit_exons, corresponding exons that contain the variant
        """
        ctx_name = data.get_canonical_transcript(self.gene.gene_name)
        ctx_var_chromosome_position = 0
        results = []
        skips = []
        hit_exons = []
        donor_exon_bases = self.config.getint("splice_scoring_tool", "donor_exon_bases")
        acceptor_exon_bases = self.config.getint("splice_scoring_tool", "acceptor_exon_bases")

        for tx in self.gene.transcripts:
            if tx.tx_name == ctx_name:
                ctx_var_chromosome_position = tx.cdna_to_chrom[self.var_cdna_pos]
                break

        if self.gene.strand == '+':
            for tx in self.gene.transcripts:
                skipped = True
                if self.var_cdna_pos not in tx.cdna_to_chrom:
                    skips.append(tx.tx_name)
                    continue
                for exon in tx.exons:
                    if ctx_var_chromosome_position in range(exon.start, exon.start + acceptor_exon_bases):
                        skipped = False
                        self.acceptor = True
                        results.append(tx)
                        hit_exons.append(exon)
                        break
                    elif ctx_var_chromosome_position in range(exon.end - donor_exon_bases + 1, exon.end + 1):
                        skipped = False
                        results.append(tx)
                        hit_exons.append(exon)
                        self.donor = True
                        break
                if skipped:
                    skips.append(tx.tx_name)

        elif self.gene.strand == '-':
            for tx in self.gene.transcripts:
                skipped = True
                if self.var_cdna_pos not in tx.cdna_to_chrom:
                    skips.append(tx.tx_name)
                    continue
                for exon in tx.exons:
                    if ctx_var_chromosome_position in range(exon.start - (acceptor_exon_bases - 1), exon.start + 1):
                        skipped = False
                        self.acceptor = True
                        results.append(tx)
                        hit_exons.append(exon)
                        break
                    elif ctx_var_chromosome_position in range(exon.end, exon.end + donor_exon_bases):
                        skipped = False
                        results.append(tx)
                        hit_exons.append(exon)
                        self.donor = True
                        break
                if skipped:
                    skips.append(tx.tx_name)

        return skips, results, hit_exons

class SpliceVariantData:
    def __init__(self, splice_variant, tx, exon):
        """
        This object holds data for each of the transcripts contained in the tx_hits list in the SpliceVariant object
        :param splice_variant: (splice variant object)
        :param tx: (transcript object)
        :param exon: (exon object)
        :return: (splice variant data object)
        """
        self.config = splice_variant.config
        self.donor_exon_bases = self.config.getint("splice_scoring_tool", "donor_exon_bases")
        self.acceptor_exon_bases = self.config.getint("splice_scoring_tool", "acceptor_exon_bases")
        self.donor_intron_bases = self.config.getint("splice_scoring_tool", "donor_intron_bases")
        self.acceptor_intron_bases = self.config.getint("splice_scoring_tool", "acceptor_intron_bases")
        self.search_upstream = self.config.getint("splice_scoring_tool", "search_upstream")
        self.search_downstream = self.config.getint("splice_scoring_tool", "search_downstream")
        self.threshold = self.config.getfloat("splice_scoring_tool", "threshold")
        self.tx = tx
        self.exon = exon
        (self.wt_site,
         self.var_site,
         self.upstream_seq,
         self.downstream_seq) = self.get_sequences(splice_variant)
        self.exceed_threshold = False
        (self.wt_score,
         self.var_score,
         self.upstream_scores,
         self.downstream_scores) = self.score_splice_sites(splice_variant)

    def get_sequences(self, splice_variant):
        """
        Gets the dna sequence for the wild type splice site, the same site with the variant nucleotide introduced,
        the sequences upstream and downstream from the splice site, as specified in the config file
        :param splice_variant: (splice variant object)
        :return: (string) wt_site
                 (string) var_site
                 (string) upstream_seq
                 (string) downstream_seq
        """
        gene = splice_variant.gene
        wt_site = ''
        var_site = ''
        upstream_seq = ''
        downstream_seq = ''
        mapped_seq = self.tx.mapped_sequence
        start = self.exon.start
        end = self.exon.end
        var_loc = self.tx.cdna_to_chrom[splice_variant.var_cdna_pos]

        if gene.strand == '-':

            if splice_variant.donor:

                if splice_variant.distance_from_exon:
                    var_loc = var_loc - splice_variant.distance_from_exon
                begin_loc = end - self.donor_intron_bases
                end_loc = end + self.donor_exon_bases
                read_range = xrange(begin_loc, end_loc)
                for i in reversed(read_range):
                    wt_site += mapped_seq[i]
                    if i == var_loc:
                        var_site += splice_variant.var_nuc
                    else:
                        var_site += mapped_seq[i]

            elif splice_variant.acceptor:

                if splice_variant.distance_from_exon:
                    var_loc = var_loc + splice_variant.distance_from_exon
                begin_loc = start - self.acceptor_exon_bases + 1
                end_loc = start + self.acceptor_intron_bases + 1
                read_range = xrange(begin_loc, end_loc)
                for i in reversed(read_range):
                    wt_site += mapped_seq[i]
                    if i == var_loc:
                        var_site += splice_variant.var_nuc
                    else:
                        var_site += mapped_seq[i]

            upstream_distance = var_loc + self.search_upstream
            downstream_distance = var_loc - self.search_downstream
            for i in reversed(xrange(var_loc, upstream_distance)):
                upstream_seq += mapped_seq[i]
            for i in reversed(xrange(downstream_distance, var_loc)):
                downstream_seq += mapped_seq[i]

        elif gene.strand == '+':

            if splice_variant.donor:

                if splice_variant.distance_from_exon:
                    var_loc = var_loc + splice_variant.distance_from_exon
                begin_loc = end - self.donor_exon_bases + 1
                end_loc = end + self.donor_intron_bases + 1
                read_range = xrange(begin_loc, end_loc)
                for i in read_range:
                    wt_site += mapped_seq[i]
                    if i == var_loc:
                        var_site += splice_variant.var_nuc
                    else:
                        var_site += mapped_seq[i]

            elif splice_variant.acceptor:

                if splice_variant.distance_from_exon:
                    var_loc = var_loc - splice_variant.distance_from_exon
                begin_loc = start - self.acceptor_intron_bases
                end_loc = start + self.acceptor_exon_bases
                read_range = xrange(begin_loc, end_loc)
                for i in read_range:
                    wt_site += mapped_seq[i]
                    if i == var_loc:
                        var_site += splice_variant.var_nuc
                    else:
                        var_site += mapped_seq[i]

            upstream_distance = var_loc - self.search_upstream
            downstream_distance = var_loc + self.search_downstream
            for i in xrange(var_loc, upstream_distance):
                upstream_seq += mapped_seq[i]
            for i in xrange(downstream_distance, var_loc):
                downstream_seq += mapped_seq[i]

        if splice_variant.donor:
            wt_site = wt_site[0:self.donor_exon_bases].upper() + wt_site[self.donor_exon_bases:].lower()
            var_site = var_site[0:self.donor_exon_bases].upper() + var_site[self.donor_exon_bases:].lower()
        elif splice_variant.acceptor:
            wt_site = wt_site[0:self.acceptor_intron_bases].lower() + wt_site[self.acceptor_intron_bases:].upper()
            var_site = var_site[0:self.acceptor_intron_bases].lower() + var_site[self.acceptor_intron_bases:].upper()

        return wt_site, var_site, upstream_seq, downstream_seq

    def score_splice_sites(self, splice_variant):
        """
        Uses a scoring algorithm to score the wild-type splice site, splice site with the variant introduced,
        the sites upstream and the sites downstream from the splice site
        :param splice_variant: (splice variant object)
        :return: (float) wt_score
                 (float) var_score
                 (list of floats) upstream_scores
                 (list of floats) downstream_scores
        """
        scoring_tool = self.config.get("splice_scoring_tool", "tool")
        scoring_method = getattr(data, scoring_tool + "_score")
        seq_size = 0
        sites_to_score = []
        downstream_scores = []
        upstream_scores = []
        if splice_variant.acceptor:
            seq_size = self.acceptor_exon_bases + self.acceptor_intron_bases
        elif splice_variant.donor:
            seq_size = self.donor_exon_bases + self.donor_intron_bases

        sites_to_score.append(self.wt_site)
        sites_to_score.append(self.var_site)
        scoring_result = scoring_method(acceptor=splice_variant.acceptor, donor=splice_variant.donor, to_score=sites_to_score)
        wt_score = float(scoring_result[0].split('\t')[1])
        var_score = float(scoring_result[1].split('\t')[1])

        if var_score < self.threshold:
            self.exceed_threshold = True
            del sites_to_score[:]
            for i in range(0, len(self.upstream_seq) - seq_size + 1):
                sites_to_score.append(self.upstream_seq[i:(i + seq_size)])
            scoring_result = scoring_method(acceptor=splice_variant.acceptor, donor=splice_variant.donor, to_score=sites_to_score)

            for entry in scoring_result[:-1]:
                res = entry.split('\t')
                score = float(res[1])
                site = res[0]
                upstream_scores.append((score, site))

            del sites_to_score[:]
            for i in range(0, len(self.downstream_seq) - seq_size):
                sites_to_score.append(self.downstream_seq[i:(i + seq_size)])
            scoring_result = scoring_method(acceptor=splice_variant.acceptor, donor=splice_variant.donor, to_score=sites_to_score)

            for entry in scoring_result[:-1]:
                res = entry.split('\t')
                score = float(res[1])
                site = res[0]
                downstream_scores.append((score, site))

        return wt_score, var_score, upstream_scores, downstream_scores
