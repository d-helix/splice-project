# Splice Variant Analysis Tool#

The SVA tool is to analyse genetic variants. Specifically it is used to analyze variants that occur in splicing regions. 


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Input Requirements ###

The tool has some requirements for input. 

python sva.py <gene name> <variant>

Variant Notation:
Currently, only single nucleotide change variants are supported by the SVA tool. 
The variant must use HGVS nomenclature and must use cDNA position. 
The variant must also be enclosed in quotes. Variants use the > symbol which is also a linux command to send the output to a file. To prevent incorrect input and prevent unintentionally writing to file, enclose the variant in quotes.

Example: python.py CERKL "c.613G>T"

### Improvements/Next Steps ###
* Add error handling. Catch errors and print out helpful messages on failures.
* 