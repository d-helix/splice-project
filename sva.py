import argparse
import re
import ConfigParser
from modules import gene
from modules import data
from modules import variant
from modules import results

def create_gene(gene_name):
    """
    Creates a gene object using refseq data
    :param gene_name: (string) HGNC approved gene symbol
    :return: gene object
    """
    gene_info, tx_list = data.get_refseq_data(gene_name)
    g = gene.Gene(gene_info, tx_list)

    return g

def create_variant(gene_obj=None, splice=None, other_variant=None, config_obj=None):
    """
    Creates a variant object
    :param gene_obj: (gene) object containing gene data
    :param splice: (string) HGVS formatted splice variant
    :param other_variant: (string) HGVS formatted variant
    :param config_obj: object containing configuration data
    :return: variant object
    """
    v_input = splice if other_variant is None else other_variant
    re_search = re.search(r'c.(\d+)([-+]*\d*)([GTAC])>([GTAC])', v_input)
    cdna_pos = int(re_search.group(1))
    distance = None

    if re_search.group(2):
        distance = int(re_search.group(2))
    wt_nuc = re_search.group(3)
    var_nuc = re_search.group(4)

    var = None
    if splice:
        var = variant.SpliceVariant(config_obj, gene_obj, cdna_pos, distance, wt_nuc, var_nuc)
    elif other_variant:
        var = variant.Variant(config_obj, gene_obj, cdna_pos, distance, wt_nuc, var_nuc)

    return var

def get_args():
    """
    Pulls the arguments off the commandline and parses using rules
    :return: parser object
    """
    parser = argparse.ArgumentParser(description="Performs various variant analysis tasks, "
                                                 "including splice site analysis")
    parser.add_argument('gene', help='The gene of interest')
    parser.add_argument('variant', help='Variant to be analyzed')

    group = parser.add_mutually_exclusive_group()
    group.add_argument('-s', '--splice', metavar='variant', help="Specifies that a splice variant is being input")
    group.add_argument('-v', '--variant', nargs=1, metavar='variant', help='Test a variant other than a splice variant')
    group.add_argument('-g', help='Get information for a gene')

    return parser.parse_args()

def get_config():
    """
    Retrieves data from the configuration file and builds a config object
    :return: configuration object
    """
    config_file = "config.ini"
    conf = ConfigParser.ConfigParser()
    conf.read(config_file)

    return conf

if __name__ == '__main__':

    config = get_config()
    args = get_args()
    gene_obj = create_gene(args.gene)
    variant_obj = create_variant(gene_obj=gene_obj, splice=args.variant, config_obj=config)
    result = results.SpliceResult(variant_obj, gene_obj, config, args)
    result.print_results()