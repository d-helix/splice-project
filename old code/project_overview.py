### Project Outline for Splice Variant Analysis ### Derek Nielsen

#===========< DATA >=================#
'''
hg19 genome data was used, chromosome fasta files were downloaded from
    http://hgdownload.soe.ucsc.edu/goldenPath/hg19/chromosomes/

''' 

#------ gene.py ---------------------#
def load_gene_info():
    '''loads the info from the refSeq file into Gene objects'''
class Gene:
	name                
	strand             
	chromosome
	transcripts = []  

class Transcript:
    exons = []
    tx_start
    tx_end
    cds_start
    cds_end
   
class Exon:
    start
    stop
    frame
    length


#------ splicesp.py ------------------#
def test_current_site():
    '''returns a score for the splice site in question'''
def find_alternate_acceptor_sites():
    '''returns a list of alternate acceptor sites that meet threshold requirements''' 
def find_alternate_donor_sites():
    '''returns a list of alternate donor sites that meet threshold requirements'''


#------ dataloader.py ----------------#
def command_line_check():
    '''Checks that command line arguments are valid'''
def parse_command_line():
    '''COMMAND LINE USAGE: ./splicevariant.py gene_name variant_name [options]
           Variant name must be cDNA position in HGVS nomenclature
           Options:
                -t 3.6  Threshold to use for splice site recognition '''
def create_new_gene():
    '''creates a new Gene onject for the appropriate gene from the command line'''
def get_sequence():    
    '''using data from the Gene object, loads a sequence from the appropriate 
       chromosome file. Grabs a sequence of about 200 bp's, 100 upstream and 100 downstream''' 


#------ saa.py (main) ------#









