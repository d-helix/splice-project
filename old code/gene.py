#!/usr/bin/python

''' 
This Gene object will hold all of the data for the gene to be evaluated, including all
of its transcripts and their exons. 
When this py file is run aside from the main ssa program it will print all of the 
refSeq data for a particular gene.
    USAGE: ./gene.py gene_name 
'''

import sys
import os
import re
import subprocess
import string


#------------- Data ----------------#
codons = {"ATT":"I","ATC":"I","ATA":"I",
          "CTT":"L","CTC":"L","CTA":"L","CTG":"L","TTA":"L","TTG":"L",
          "GTT":"V","GTC":"V","GTA":"V","GTG":"V",
          "TTT":"F","TTC":"F",
          "ATG":"M",
          "TGT":"C","TGC":"C",
          "GCT":"A","GCC":"A","GCA":"A","GCG":"A",
          "GGT":"G","GGC":"G","GGA":"G","GGG":"G",
          "CCT":"P","CCC":"P","CCA":"P","CCG":"P",
          "ACT":"T","ACC":"T","ACA":"T","ACG":"T",
          "TCT":"S","TCC":"S","TCA":"S","TCG":"S","AGT":"S","AGC":"S",
          "TAT":"Y","TAC":"Y",
          "TGG":"W",
          "CAA":"Q","CAG":"Q",
          "AAT":"N","AAC":"N",
          "CAT":"H","CAC":"H",
          "GAA":"E","GAG":"E",
          "GAT":"D","GAC":"D",
          "AAA":"K","AAG":"K",
          "CGT":"R","CGC":"R","CGA":"R","CGG":"R","AGA":"R","AGG":"R",
          "TAA":"X","TAG":"X","TGA":"X"}


#------------- Classes -------------#

class Gene:

    def __init__(self,gene_info,tx_list):
        self.gene_name   =   gene_info[0]
        self.chromosome  =   gene_info[1]
        self.strand      =   gene_info[2]

        self.transcripts = []
        for tx in tx_list:
            self.transcripts.append(Transcript(self,tx))

    def print_gene(self):
        print self.gene_name
        print self.chromosome
        print 'Strand: ' + self.strand
        print '  Transcripts:'
        for tx in self.transcripts:
            print '  ' + tx.tx_name
            print '  tx start: '  + tx.tx_start
            print '  tx end:   '  + tx.tx_end
            print '  cds start: ' + str(tx.cds_start)
            print '  cds_end:   ' + str(tx.cds_end)
            print '    Exons:'
            for ex in tx.exons:
                print ('     ' + str(ex.exon_number) + ': ' + str(ex.start) + ' - ' +  
                       str(ex.end) + '    frame: ' + ex.frame + '  length: ' + str(ex.length)) 


class Transcript:

    def __init__(self,gene,tx):
        self.tx_name    =  tx[1]
        self.tx_start   =  tx[4]
        self.tx_end     =  tx[5]
        self.cds_start  =  int(tx[6]) + 1
        self.cds_end    =  int(tx[7])
        self.no_amino_acids = 0
        if gene.strand == '-':
            self.tx_start, self.tx_end = self.tx_end, self.tx_start
            self.cds_start, self.cds_end = self.cds_end, self.cds_start 

        self.exons  =  []
        exon_starts =  tx[9].split(',')[:-1]
        exon_ends   =  tx[10].split(',')[:-1]
        exon_frames =  tx[15].split(',')[:-1]
        exon_count  =  int(tx[8])
        exon_range  =  list(range(exon_count))
        if gene.strand == '-':
            exon_range = list(reversed(exon_range))
        exon_num = 1
        for i in exon_range:
            self.exons.append(
                  Exon(gene,exon_starts[i],exon_ends[i],exon_frames[i],exon_num))
            exon_num += 1
        
        self.cDNA_pos = {}
        self.get_cDNA_pos(gene)

    def get_cDNA_pos(self,gene):
        pos = 1
        if gene.strand == '+':
            for exon in self.exons:
                if exon.frame == '-1':
                    continue
                elif pos == 1:
                    for i in range(self.cds_start,exon.end + 1):
                        self.cDNA_pos[pos] = i
                        pos += 1
                else:
                    for i in range(exon.start, exon.end + 1):
                        if i > self.cds_end:
                            break
                        self.cDNA_pos[pos] = i
                        pos += 1
        elif gene.strand == '-':
            for exon in self.exons:
                if exon.frame == '-1':
                    continue
                elif pos == 1:
                    for i in range(self.cds_start,exon.end - 1,-1):
                        self.cDNA_pos[pos] = i
                        pos += 1
                else:
                    for i in range(exon.start, exon.end - 1,-1):
                        if i < self.cds_end:
                            break
                        self.cDNA_pos[pos] = i
                        pos += 1
        self.no_amino_acids = pos/3
        print self.no_amino_acids 

class Exon:

    def __init__(self,gene,start,end,frame,exon_num):
        if gene.strand == '+':
            self.start  =  int(start) + 1
            self.end    =  int(end)
        elif gene.strand == '-':
            self.start = int(end)
            self.end   = int(start) + 1
        self.frame  =  frame
        self.length =  abs( int(end) - int(start) ) 
        self.exon_number = exon_num


class Variant:

    def __init__(self,v_input,gene):
        self.v = v_input
        reSearch = re.search(r'c.(\d+)([-+]*\d*)([GTAC])>([GTAC])',v_input)
        self.variant_cDNA_pos  = int(reSearch.group(1))
        self.distance = 0
        if reSearch.group(2):
            self.distance  = int(reSearch.group(2))
        self.wt_nuc = reSearch.group(3)
        self.var_nuc   = reSearch.group(4)
        self.donor     = False
        self.acceptor  = False
        self.wt_site      = ''
        self.variant_site = ''
        self.tx_splice_sites = []
        self.skips,self.results = self.find_site(gene)

    def find_site(self,gene):
        # this is for forward strand genes. 
        results = []
        skips = []
        for tx in gene.transcripts:
            
            skipped = True
            for exon in tx.exons:
                curr_pos = tx.cDNA_pos[self.variant_cDNA_pos]
                if curr_pos in range(exon.start,exon.start + 6):
                    skipped = False
                    self.acceptor = True
                    self.tx_splice_site.append(tx)
                    self.get_results(curr_pos)
                    break
                elif curr_pos in range(exon.end - 6,exon.end + 1):
                    skipped = False
                    self.tx_splice_sites.append(tx)
                    self.donor = True
                    results.append(Result(self,curr_pos,exon,gene,tx))
                    break
            if skipped == True: # true if the exon is skipped in the transcript so that the variant is found in an intronic region of the transcript
                skips.append(tx.tx_name)
        return skips,results

class Result:
    def __init__(self,variant,curr_pos,exon,curr_gene,tx):
        
        self.transcript_name = tx.tx_name
        self.wt_site = ''
        self.variant_site = ''
        self.wt_score = -1
        self.variant_score = -1
        self.upDNA = ''
        self.dnDNA = ''
        self.upstream_results = []
        self.downstream_results = []
        self.donacc = ''
        self.ex_num = exon.exon_number

        file_directory = os.getcwd()
        chr_file = curr_gene.chromosome + '.fa'
        with open (file_directory + '/files/chromosomes/' + chr_file) as fh:
            if variant.donor == True:
                donacc = 'Donor'
                begin = curr_pos - 100
                end   = curr_pos + 100
                seq   = ''
                for line in fh:
                    seq += line.strip()
                    if len(seq) > end:
                        seq = '-' + seq
                        break

                upstream = seq[begin+1:exon.end+1]
                self.upDNA = upstream
                downstream = seq[exon.end+1:end + 2].lower()
                self.dnDNA = downstream
                self.wt_site = (seq[exon.end - 2:exon.end + 1].upper() + 
                               seq[exon.end + 1:exon.end + 7].lower())

                if variant.distance == 0:
                    if curr_pos == exon.end:
                        self.variant_site = (seq[exon.end - 2:exon.end].upper() + 
                                  variant.var_nuc.upper() + seq[exon.end + 1:exon.end + 7].lower())
                    else:
                        vp = tx.cDNA_pos[variant.variant_cDNA_pos]
                        if vp == exon.end - 2:
                            self.variant_site = (variant.var_nuc.upper() + seq[vp + 1:exon.end + 1].upper()
                                            + seq[exon.end + 1:exon.end + 7].lower())
                        else:
                            self.variant_site = (seq[exon.end - 2].upper() + variant.var_nuc.upper() 
                                        + seq[exon.end].upper() + seq[exon.end + 1:exon.end + 7].lower())
                else:
                    dis = variant.distance + 2
                    self.variant_site = (self.wt_site[:3] + self.wt_site[3:dis] + variant.var_nuc.lower() 
                                    + self.wt_site[dis + 1:])

                totest = open('maxent/torun','w')
                totest.write(self.wt_site + '\n')
                totest.write(self.variant_site + '\n')
                totest.close()
                output = subprocess.check_output(["perl","maxent/score5.pl","maxent/torun"])
                output = output.split('\n')
                
                self.wt_score = output[0].split()[1]
                self.variant_score = output[1].split()[1]

                totest = open('maxent/torun','w')
                for i in range(len(upstream) - 9):
                    totest.write(upstream[i:i+9]+'\n')
                totest.close()
                self.upstream_results = subprocess.check_output(["perl","maxent/score5.pl","maxent/torun"])
                self.upstream_results = self.upstream_results.split('\n')


                totest = open('maxent/torun','w')
                for i in range(len(downstream) - 9):
                    totest.write(downstream[i:i+9]+'\n')
                totest.close()
                self.downstream_results = subprocess.check_output(["perl","maxent/score5.pl","maxent/torun"])
                self.downstream_results = self.downstream_results.split('\n')

#---------------------------------------------------------#

def find_peptide(sequence,codons):
    peptide = ''
    codon_list = re.findall('.{1,3}',sequence)
    for c in codon_list:
        peptide += codon_list[c]
    return peptide  

def print_results(variant,gene,threshold):


#protein = find_peptide(sequence,codons)


    results = variant.results
    skips = variant.skips
    final = ''
    threshold = float(threshold)

    final += 'Gene: ' + gene.gene_name + '\n'
    final += 'Variant: ' + variant.v + '\n'
    final += 'Threshold: ' + str(threshold) + '\n\n'
    final += str(len(skips)) + ' transcripts skip the variant site.\n'
    final += '   ' + ','.join(skips) + '\n'

    for r in results:
    
        frame_mod = variant.variant_cDNA_pos % 3
        print "Exonic: " + r.upDNA
        print "Intronic: " + r.dnDNA
        final += 'Transcript: ' + r.transcript_name + '\n'
        final += 'Splice Site Scores:\n'
        final += '  Wild-type: ' + r.wt_site + ' ' + r.wt_score + '\n'
        final += '  Varaint:   ' + r.variant_site + ' ' + r.variant_score + '\n'

        if float(r.variant_score) > threshold:
            if float(r.variant_score) > float(r.wt_score): 
                final += 'Variant splice site score exceeds wild-type score.\n'
            else:
                final += ('Variant splice site score reduced by ' 
                       +  str(float(r.wt_score) - float(r.variant_score))
                       +  ' but still exceeds threshold\n')
        else:
            final += 'Splice site abolished. '

            save_up = []
            bps_away = 0
            bp_array = []
            for line in r.upstream_results:
                if line == '':
                    continue
                line = line.strip().split()
                if float(line[1]) > threshold:
                    bp_array.append(bps_away)
                    save_up.append(line)
                bps_away += 1
            
            save = []
            bps_awy = 0
            bp_arr = []

            for line in r.downstream_results:
                if line == '':
                    continue
                line = line.strip().split()
                if float(line[1]) > threshold:
                    bp_arr.append(bps_awy)
                    save.append(line)
                bps_awy += 1

            if (len(save_up) == 0) and (len(save) == 0):
                final += "No alternative sites score above the threshold.\n"
            else:
                final += "Possible alternative sites listed below.\n"

            start = float(variant.variant_cDNA_pos)/3.0
            print 'Start: ' + str(start)

            for i in range(len(save_up)):
                final += '  ' + str(bp_array[i]) + ' bp upstream: ' + '  '.join(save_up[i]) + '\n'
                if (bp_array[i]%3) != 0:
                    final += '    Will cause frameshift, '  
                else:
                    final += '    Same frame, '
                final += str((bp_array[i] + 1) / 3) + ' amino acids added\n'

            for i in range(len(save)):
                final += '  ' + str(bp_arr[i]) +  ' bp downstream: ' + '  '.join(save[i]) + '\n'
                if (bp_arr[i]%3) != 0:
                    final += '    Will cause frameshift, '  
                else:
                    final += '    Same frame'
                final += str((bp_arr[i] + 1) / 3) + ' amino acids added\n'

    print final
    return final


# -------------- Main ---------------- #

if __name__ == '__main__':
    gene_name = sys.argv[1]
    file_directory = os.getcwd()

    with open (file_directory + '/files/refSeqData') as fh:
        tx_list = []
        for line in fh:
            line = line.strip().split()
            if line[12] == gene_name:
                tx_list.append(line)
        if len(tx_list) == 0:
            sys.exit('Gene not found, please check gene name')
        rS = tx_list[0]
        gene_info = [rS[12],rS[2],rS[3]]

    gene = Gene(gene_info,tx_list)
    gene.print_gene()


