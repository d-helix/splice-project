#!/usr/bin/python

''' 
Data found in each line of the refSeq file, tab delimited
0   bin# 
1   name     
2   chrom     
3   strand     
4   txStart    
5   txEnd      
6   cdsStart   
7   cdsEnd     
8   exonCount  
9   exonStarts 
10  exonEnds   
11  score     
12  name2      
13  cdsStartStat
14  cdsEndStat 
15  exonFrames 

Coordinates always have a zero-based start and a one-based end
'''

import sys
import os

import gene


# -------------- Global Variables ------------ #

usage_statement = 'USAGE: ./splicevariant.py gene_name variant_name [options]\n' +\
                  '       Variant must be in HGVS nomenclature\n' +\
                  '       For more info and a list of options: ./splicevariant.py --help'

help_info = 'This is where the help info will eventualy go'


# ===============<[ Functions ]>=============== #

def main(arg):
    command_line_check(arg)
    options = parse_command_line(arg)
    curr_gene = create_gene(arg[1])
    sequence = get_sequence(curr_gene,arg[2])
    return curr_gene,sequence,options

def command_line_check(arg):
    if arg[1] == '--help':
        sys.exit(help_info)
    if len(arg) < 3:
        sys.exit('**Command Line Parameter Error**\n' + usage_statement)

def parse_command_line(arg):
   
    return '-1'

def create_gene(gene_name):
    file_directory = os.getcwd()
    with open (file_directory + '/files/refSeqData') as fh:
        tx_list = []
        for line in fh:
            line = line.strip().split()
            if line[12] == gene_name:
                tx_list.append(line)
        if len(tx_list) == 0:
            sys.exit('Gene not found, please check gene name')

        rS = tx_list[0]
        gene_info = [rS[12],rS[2],rS[3]]

    return gene.Gene(gene_info,tx_list)

def get_sequence(curr_gene,v_input):

    variant  = gene.Variant(v_input,curr_gene)
    
    file_directory = os.getcwd()
    chr_file = curr_gene.chromosome + '.fa'
    with open (file_directory + '/files/chromosomes/' + chr_file) as fh:
        begin = variant.cDNA_pos - 100
        end   = variant.cDNA_pos + 100
        seq   = ''
        for line in fh:
            seq = seq + line.strip()
            if len(seq) > end:
                break
        print seq[begin:variant.cDNA_pos+1]
        print seq[variant.cDNA_pos:end]
    
    return 'ATCG'


